<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Medico;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;

class MedicosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $medicos = Medico::all()->map(function ($medico) {
        return [
            'id' => $medico->id,
            'nombre' => $medico->nombre,
            'apellido' => $medico->apellido,
            'especialidad' => $medico->especialidad,
            // 'edit_url' => URL::route('medicos.edit', $medico),
            //genera la url para edicion con parametro de entrada que 
            //puede usarse con inertia-link
        ];
      });
      return Inertia::render('Medicos/Index', [
        'medicos' => $medicos
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Medicos/Agregar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
      Medico::create(
        Request::validate([
        'nombre' => 'required|max:50',
        'apellido' => 'required|max:50',
        'especialidad' => 'required|max:50'
        ])
      );

        return redirect('/medicos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Medico $medico)
    {
      // dd($medico);
      $fichas = Medico::find($medico->id)->fichas
        ->map(function ($ficha){
          return [
            'id' => $ficha->id,
            'dia' => $ficha->dia,
            'hora' => $ficha->hora,
            'medico_id' => $ficha->medico_id
          ];
        });
      return Inertia::render('Medicos/Mostrar', [
        'medico' => [
          'id' => $medico->id,
          'nombre' => $medico->nombre,
          'apellido' => $medico->apellido,
          'especialidad' => $medico->especialidad
        ],
        'fichas' => $fichas
      ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Medico $medico)
    {
        return Inertia::render('Medicos/Editar', [
          'medico' => [
            'id' => $medico->id,
            'nombre' => $medico->nombre,
            'apellido' => $medico->apellido,
            'especialidad' => $medico->especialidad
          ]
        ]);
        // dd($medico->nombre);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Medico $medico)
    {
      // dd($medico);
      $medico->update(
        Request::validate([
          'nombre' => 'required|max:50',
          'apellido' => 'required|max:50',
          'especialidad' => 'required|max:50'
        ])
      );

      return Redirect::route('medicos')->with('success', 'medico editado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Medico $medico)
    {
      $medico->delete();

      return Redirect::route('medicos');
    }
}
