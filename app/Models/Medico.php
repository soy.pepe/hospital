<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Medico extends Model
{
    use HasFactory;

    protected $table = 'medicos';

    protected $primaryKey = 'id';

    protected $fillable = [
      'nombre',
      'apellido',
      'especialidad'
    ];

    protected $hidden = [
      'id',
      'created_at',
      'updated_at'
    ];

    protected $visible = [
      'nombre',
      'apellido',
      'especialidad'
    ];

    //relacion con la tabla fichas
    public function fichas(){
      return $this->hasMany(Ficha::class);
    }
}
