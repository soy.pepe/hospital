<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ficha extends Model
{
    use HasFactory;

    protected $table = 'fichas';

    protected $primaryKey = 'id';

    protected $fillable = [
      'id',
      'medico_id',
      'paciente_id',
      'fecha',
      'hora'
    ];

    protected $visible = [
      'medico_id',
      'dia',
      'hora'
    ];

    protected $hidden = [
      'id',
      'created_at',
      'updated_at'
    ];

    //relacion con la tabla medicos
    public function medico(){
      return $this->belongsTo(Medico::class);
    }
}
