<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => '¡Su contraseña fue reestablecida!',
    'sent' => '¡Le enviamos a su correo electrónico el enlace de reinicio de contraseña!',
    'throttled' => 'Espere antes de reintentar.',
    'token' => 'El token de la contraseña es inválido.',
    'user' => "No podemos encontrar a un usuario con ese correo electrónico.",

];
