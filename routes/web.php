<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\MedicosController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Inicio', [
        // 'canLogin' => Route::has('login'),
        // 'canRegister' => Route::has('register'),
        // 'laravelVersion' => Application::VERSION,
        // 'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/hola', function() {
    return Inertia::render('hola');
})->name('hola');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->name('dashboard');

// Route::get('/fichas', function(){
//     return Inertia::render('Fichas');
// })->middleware('auth');

Route::get('medicos', [MedicosController::class, 'index'])
  ->name('medicos');

Route::get('medicos/{medico}', [MedicosController::class, 'show'])
  ->name('medicos.mostrar');

Route::get('medicos/agregar', [MedicosController::class, 'create'])
  ->name('medicos.agregar');

Route::post('medicos', [MedicosController::class, 'store'])
  ->name('medicos.store');

Route::get('medicos/{medico}/editar', [MedicosController::class, 'edit'])
  ->name('medicos.editar');

Route::put('medicos/{medico}', [MedicosController::class, 'update'])
  ->name('medicos.update');

Route::delete('medicos/{medico}', [MedicosController::class, 'destroy'])
  ->name('medicos.eliminar');

